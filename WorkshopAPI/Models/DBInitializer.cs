﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkshopAPI.Models;
using static WorkshopAPI.Startup;

namespace WorkshopAPI.Data
{
    public class DBInitializer : IDInitializer
    {
        private readonly ProductDbContext _context;
        public DBInitializer(ProductDbContext context)
        {
            _context = context;
        }
        public async Task DataSeeding()
        {
            try
            {
                await _context.AddRangeAsync(new List<Product> {
                    new Product {ProductName ="เอสเปคโต ปราโตนุม ", IsDeleted = false},
                    new Product {ProductName ="เอสเปสโซ ร้อน ", IsDeleted = false},
                    new Product {ProductName ="เอสเปสโซ เย็น ", IsDeleted = false},
                    new Product {ProductName ="เอสเปสโซ ปั่น ", IsDeleted = false},
                    new Product {ProductName ="อเมซอน ร้อน ", IsDeleted = false},
                    new Product {ProductName ="อเมซอน เย็น ", IsDeleted = false},
                    new Product {ProductName ="อเมซอน ปั่น ", IsDeleted = false},
                    new Product {ProductName ="ลาเต้ ร้อน ", IsDeleted = false},
                    new Product {ProductName ="ลาเต้ เย็น ", IsDeleted = false},
                    new Product {ProductName ="ลาเต้ ปั่น ", IsDeleted = false},
                    new Product {ProductName ="มอคค่า ร้อน ", IsDeleted = false},
                    new Product {ProductName ="มอคค่า เย็น ", IsDeleted = false},
                    new Product {ProductName ="มอคค่า ปั่น ", IsDeleted = false},

                });

                await _context.AddRangeAsync(new List<ProductDetail>
                {
                    new ProductDetail {ProductDetailID = "A01", ProductId = 1, ProductDetailName="กาแฟ", ProductDetailPrice = 35},
                    new ProductDetail {ProductDetailID = "A02", ProductId = 2, ProductDetailName="กาแฟ", ProductDetailPrice = 45},
                    new ProductDetail {ProductDetailID = "A03", ProductId = 3, ProductDetailName="กาแฟ", ProductDetailPrice = 50},
                    new ProductDetail {ProductDetailID = "A04", ProductId = 4, ProductDetailName="กาแฟ", ProductDetailPrice = 55},
                    new ProductDetail {ProductDetailID = "A05", ProductId = 5, ProductDetailName="กาแฟ", ProductDetailPrice = 35},
                    new ProductDetail {ProductDetailID = "A06", ProductId = 6, ProductDetailName="กาแฟ", ProductDetailPrice = 45},
                    new ProductDetail {ProductDetailID = "A07", ProductId = 7, ProductDetailName="กาแฟ", ProductDetailPrice = 55},
                    new ProductDetail {ProductDetailID = "A08", ProductId = 8, ProductDetailName="กาแฟ", ProductDetailPrice = 35},
                    new ProductDetail {ProductDetailID = "A09", ProductId = 9, ProductDetailName="กาแฟ", ProductDetailPrice = 45},
                    new ProductDetail {ProductDetailID = "A10", ProductId = 10, ProductDetailName="กาแฟ", ProductDetailPrice = 55},
                    new ProductDetail {ProductDetailID = "A11", ProductId = 11, ProductDetailName="กาแฟ", ProductDetailPrice = 35},
                    new ProductDetail {ProductDetailID = "A12", ProductId = 12, ProductDetailName="กาแฟ", ProductDetailPrice = 45},
                    new ProductDetail {ProductDetailID = "A13", ProductId = 13, ProductDetailName="กาแฟ", ProductDetailPrice = 5},

                });

                await _context.AddRangeAsync(new List<Transaction>
                {
                    new Transaction {UserId = 1 , ReceivedMoney = 100, ReturnMoney = 65, TransactionDate = DateTime.Now},
                    new Transaction {UserId = 2 , ReceivedMoney = 100, ReturnMoney = 55, TransactionDate = DateTime.Now},
                    new Transaction {UserId = 3 , ReceivedMoney = 1000, ReturnMoney = 950, TransactionDate = DateTime.Now},
                    new Transaction {UserId = 4 , ReceivedMoney = 500, ReturnMoney = 445, TransactionDate = DateTime.Now},
                    new Transaction {UserId = 5 , ReceivedMoney = 100, ReturnMoney = 65, TransactionDate = DateTime.Now},
                    new Transaction {UserId = 6 , ReceivedMoney = 100, ReturnMoney = 55 , TransactionDate = DateTime.Now},
                    new Transaction {UserId = 7 , ReceivedMoney = 500, ReturnMoney = 445, TransactionDate = DateTime.Now},
                    new Transaction {UserId = 8 , ReceivedMoney = 100, ReturnMoney = 65, TransactionDate = DateTime.Now},
                    new Transaction {UserId = 9 , ReceivedMoney = 1000, ReturnMoney = 955, TransactionDate = DateTime.Now},
                    new Transaction {UserId = 10 , ReceivedMoney = 500, ReturnMoney = 445, TransactionDate = DateTime.Now},
                    new Transaction {UserId = 11 , ReceivedMoney = 100, ReturnMoney = 65, TransactionDate = DateTime.Now},
                    new Transaction {UserId = 12 , ReceivedMoney = 500, ReturnMoney = 455, TransactionDate = DateTime.Now},
                    new Transaction {UserId = 13 , ReceivedMoney = 100, ReturnMoney = 445, TransactionDate = DateTime.Now},

                });

                await _context.AddRangeAsync(new List<TransactionDetail>
                {
                    new TransactionDetail {TransactionDetailId = "D1" ,TransactionId = 1,ProductDetailId= "A01" , Quantity = 10, TotalPrice= 300 , },
                    new TransactionDetail {TransactionDetailId = "D2" ,TransactionId = 2,ProductDetailId= "A02" , Quantity = 50, TotalPrice= 300 },
                    new TransactionDetail {TransactionDetailId = "D3" ,TransactionId = 3,ProductDetailId= "A03" , Quantity = 100, TotalPrice= 300 },
                    new TransactionDetail {TransactionDetailId = "D4" ,TransactionId = 4,ProductDetailId= "A04" , Quantity = 7, TotalPrice= 300 },
                    new TransactionDetail {TransactionDetailId = "D5" ,TransactionId = 5,ProductDetailId= "A05" , Quantity = 15, TotalPrice= 800 },
                    new TransactionDetail {TransactionDetailId = "D6" ,TransactionId = 6,ProductDetailId= "A06" , Quantity = 12, TotalPrice= 300 },
                    new TransactionDetail {TransactionDetailId = "D7" ,TransactionId = 7,ProductDetailId= "A07" , Quantity = 29, TotalPrice= 300 },
                    new TransactionDetail {TransactionDetailId = "D8" ,TransactionId = 8,ProductDetailId= "A08" , Quantity = 4, TotalPrice= 300 },
                    new TransactionDetail {TransactionDetailId = "D9" ,TransactionId = 9,ProductDetailId= "A09" , Quantity = 85, TotalPrice= 500 },
                    new TransactionDetail {TransactionDetailId = "D10" ,TransactionId = 10,ProductDetailId= "A10" , Quantity = 3, TotalPrice= 300 },
                    new TransactionDetail {TransactionDetailId = "D11" ,TransactionId = 11,ProductDetailId= "A11" , Quantity = 10, TotalPrice= 300 },
                    new TransactionDetail {TransactionDetailId = "D12" ,TransactionId = 12,ProductDetailId= "A12" , Quantity = 5, TotalPrice= 300 },
                    new TransactionDetail {TransactionDetailId = "D13" ,TransactionId = 13,ProductDetailId= "A13" , Quantity = 2, TotalPrice= 280 },



                });

                await _context.AddRangeAsync(new List<User>
                {
                    new User {  username = "admin1",
                    password = "admin1",
                    firstname = "Admin1",
                    lastname = "NaJa1",
                    guid = Guid.NewGuid(),
                    email = "znatthaporn.p@pttdigital.com",
                    phone = "0934192xxx" },
                     new User {  username = "admin2",
                    password = "admin2",
                    firstname = "Admin2",
                    lastname = "NaJa2",
                    guid = Guid.NewGuid(),
                    email = "znatthaporn.p@pttdigital.com",
                    phone = "0934192xxx" },
                      new User {  username = "admin3",
                    password = "admin3",
                    firstname = "Admin3",
                    lastname = "NaJa3",
                    guid = Guid.NewGuid(),
                    email = "znatthaporn.p@pttdigital.com",
                    phone = "0934192xxx" },

                });

                await _context.SaveChangesAsync();

            }
            catch { }
        }
    }
}
