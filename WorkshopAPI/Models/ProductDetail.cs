﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WorkshopAPI.Models
{
    public class ProductDetail
    {
        [Key]
        public string ProductDetailID { get; set; }
        [ForeignKey(nameof(ProductId))]
        public int ProductId { get; set; }
        public string ProductDetailName { get; set; }
        public decimal ProductDetailPrice { get; set; }


       //[ForeignKey(nameof(ProductId))]
        public Product product { get; set; }

       // public TransactionDetail transactionDetail { get; set; }
    }
}
