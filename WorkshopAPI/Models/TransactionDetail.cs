﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WorkshopAPI.Models
{
    public class TransactionDetail
    {
        
        public string TransactionDetailId { get; set; }

        [ForeignKey(nameof(TransactionId))]
        public int TransactionId { get; set; }

        [ForeignKey(nameof(ProductDetailId))]
        public string ProductDetailId { get; set; }

        public int Quantity { get; set; }
        public decimal TotalPrice { get; set; }



        public Transaction Transaction { get; set; }
 
        public ProductDetail ProductDetail { get; set; }
    }
}
