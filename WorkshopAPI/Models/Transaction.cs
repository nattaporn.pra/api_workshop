﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WorkshopAPI.Models
{
    public class Transaction
    {
        [Key]
        public int TransactionId { get; set; }

        [ForeignKey(nameof(UserId))]
        public int UserId { get; set; }
        public decimal ReceivedMoney { get; set; }
        public decimal TotalTransactionDetailPrice { get; set; }
        public decimal ReturnMoney { get; set; }

        public DateTime TransactionDate { get; set; }

       
        public User User { get; set; }

    }
}
