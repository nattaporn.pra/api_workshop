﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using WorkshopAPI.Data;
using WorkshopAPI.Models;

namespace WorkshopAPI.Controllers
{
    public class ProductController : Controller
    {

        public readonly ProductDbContext _context;
        private readonly IConfiguration _config;
        public ProductController(ProductDbContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }
        #region Get Post Put Del --
        [HttpGet("GetProducts")]
        public ActionResult<List<Product>> GetProducts()
        {
            return _context.Products.ToList();
        }

        [HttpGet("SearchProductDetails")]
        public ActionResult<List<ProductDetail>> SearchProduct([FromQuery] string search)
        {
            _context.Products.ToList();
            var result = _context.ProductDetails.Where(t => t.ProductId.ToString().Contains(search) ||
                    t.ProductDetailName.Contains(search) || t.ProductDetailPrice.ToString().Contains(search)).ToList();

            return Ok(result);

        }

        [HttpGet("SearchTransaction")]
        public ActionResult<List<Transaction>> SearchTransaction([FromQuery] string search)
        {
            _context.Users.ToList();
            var result = _context.Transactions.Where(t => t.TransactionId.ToString().Contains(search));
            var data = result.ToList();
            return data;

        }

        [HttpGet("SearchTransactionDetail")]
        public ActionResult<List<TransactionDetail>> SearchTransactionDetail([FromQuery] string search)
        {
            _context.Products.ToList();
            _context.Transactions.ToList();
            _context.Users.ToList();
            _context.ProductDetails.ToList();
            var result = _context.TransactionDetails.Where(t => t.TransactionId.ToString().Contains(search));
            var data = result.ToList();
            return data;
        }

        [HttpPost("CreateNewProduct")]
        public async Task<ActionResult<Product>> CreateNewProduct([FromBody] Product prod)
        {
            await _context.Products.AddAsync(prod);
            await _context.SaveChangesAsync();
            return prod;
        }


        [HttpPost("CreateNewProductDetail")]
        public async Task<ActionResult<ProductDetail>> CreateNewProductDetail([FromBody] ProductDetail prodDetail)
        {
            await _context.ProductDetails.AddAsync(prodDetail);
            await _context.SaveChangesAsync();
            return prodDetail;
        }

        [HttpPost("UpdateProduct/{id}")]
        public async Task<ActionResult<Product>> UpdateProduct(int id, [FromQuery] string productName, [FromQuery] bool IsDeleted)
        {
            Product result = null;
            bool isBadRequest = false;
            try
            {
                if (string.IsNullOrEmpty(productName))
                    throw new Exception("กรุณากรอกชื่อสินค้า");

                result = await _context.Products.Where(x => x.ProductId == id).FirstOrDefaultAsync();
                if (result == null)
                    throw new Exception($"ไม่พบสินค้ารหัส {id}");

                result.ProductName = productName;
                result.IsDeleted = IsDeleted;

                _context.Update(result);
                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                isBadRequest = true;
                result = new Product { ProductName = ex.Message };
            }

            if (isBadRequest)
                return BadRequest(result);

            return Ok(result);
        }

        [HttpDelete("DeleteProduct/{id}")]
        public async Task<ActionResult<ResponseMessage>> DeleteProduct(int id)
        {
            try
            {
                var prod = await _context.Products.Where(x => x.ProductId == id).FirstOrDefaultAsync();
                if (prod == null)
                    return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = $"ไม่พบรหัสสินค้า {id}" });

                _context.Remove(prod);
                await _context.SaveChangesAsync();

                return Ok(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.OK, message = $"ลบข้อมูลสินค้ารหัส {id} เรียบร้อย" });
            }
            catch (Exception ex)
            {
                return StatusCode((int)System.Net.HttpStatusCode.InternalServerError, new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.InternalServerError, message = ex.Message });
            }
        }




        #endregion
        public class ResponseMessage
        {
            public int statusCode { get; set; }
            public string message { get; set; }

        }

        public class Employee
        {
            [Required]
            public string userName { get; set; }
            [Required]
            public string password { get; set; }
            [Required]
            public string firstName { get; set; }
            [Required]
            public string lastName { get; set; }

            public string email { get; set; }
            [Required]
            public string phone { get; set; }


        }
    }
}